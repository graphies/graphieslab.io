cnvs = 320

size(cnvs, cnvs)

newPath()
moveTo((cnvs/4, 0))
lineTo((cnvs/4, cnvs/4*3))
lineTo((cnvs, cnvs/4*3))
curveTo((cnvs, cnvs/4), (cnvs/4*3, 0), (cnvs/4, 0))
closePath()
drawPath()

oval(cnvs/2,cnvs/6*5,cnvs/6,cnvs/6)

fill(1, 0, 0, 0.7)

oval(0,cnvs/2,cnvs/2,cnvs/2)

saveImage("~/Dropbox/WIP/graphies.gitlab.io/ressources/graphies-icon-01.png")
